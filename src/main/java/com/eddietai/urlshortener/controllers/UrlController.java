package com.eddietai.urlshortener.controllers;


import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.security.MessageDigest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/url")
public class UrlController {

    final static int HASH_SEGMENT_LENGTH = 12;
    final static String BASE_62_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @GetMapping("/shorten")
    public ResponseEntity<String> shortenUrl(@RequestParam("url") String url) throws NoSuchAlgorithmException {
        // Generate UUID
        String uuid = UUID.randomUUID().toString();

        // Hash UUID using SHA-256
        String hash = null;
        try {
            hash = this.getSHA256Hash(uuid);
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.internalServerError().body("Unexpected error");
        }

        String hashSegment = this.takeHashSegment(hash);

        String hashBase62 = this.convertHexToBase62(hashSegment);

        return ResponseEntity.ok().body(hashBase62);
    }

    private String getSHA256Hash(String input) throws NoSuchAlgorithmException {
        byte[] bytes = input.getBytes();

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hash = md.digest(bytes);

        // Convert bytes to hex
        StringBuilder sb = new StringBuilder();
        for (byte b : hash) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

    private String takeHashSegment(String hash) {
        return hash.substring(0, HASH_SEGMENT_LENGTH);
    }

    private String convertHexToBase62(String hexInput) {
        BigInteger decimal = new BigInteger(hexInput, 16);  // Convert hex to decimal
        return fromDecimalToBase62(decimal);
    }

    private String fromDecimalToBase62(BigInteger number) {
        StringBuilder base62 = new StringBuilder();
        BigInteger baseSize = BigInteger.valueOf(62);

        while (number.compareTo(BigInteger.ZERO) > 0) {
            BigInteger[] divmod = number.divideAndRemainder(baseSize);
            base62.insert(0, BASE_62_CHARS.charAt(divmod[1].intValue()));
            number = divmod[0];
        }
        return base62.toString();
    }
}
